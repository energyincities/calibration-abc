
from libraries import *

class DataModel():
    def __init__(self, idf=None, parameters=None, defaults=None, priors=None, meters=None, observed=None, samples_in=None, surrogate_model=None, scaler_in=None, scaler_out=None, calibration_meters=None, calibration_months=None):
        
        self.idf=idf #the building energy model .idf file, instantiated by EPPY
        self.parameters=parameters #The BESOS parameters
        self.defaults=defaults #The assumed default of the parameters
        self.priors=priors #The PYMC3 parameters
        self.meters=meters #A dictionary maaping the full-descriptive string names to the BESOS output meters
        self.observed=observed #A pandas multi-index dataframe of observed/True values
        self.samples_in=samples_in #The input parameters used in sampling in the form of a pandas dataframe, relating to the BESOS parameters
        self.surrogate_model=surrogate_model #The keras neural network surrogate model
        self.scaler_in=scaler_in #The SCIKITLEARN scalarization input, to transform the parameter inputs into a simple number for NN use
        self.scaler_out=scaler_out #The SCITKITLEARN scalarization output, to transform the sample outputs into a simpel number for NN training
        self.calibration_meters=calibration_meters
        self.calibration_months=calibration_months
        
    #In order to pickle the instance, the TF model needs to be saved separately. This saves the model based on the model's name.
    def __getstate__(self):
                            
        try:
            self.surrogate_model.save(base_location+self.surrogate_model.name+'.h5')
        except:
            print('No keras model attribute')
            
        state = self.__dict__.copy()
        
        if state['idf']==None:
            print('No idf model')
        else:
            state['idf']=True
            
        if state['parameters']==None:
            print('No parameters')
        else:
            state['parameters']=True
                            
        try:
            state['surrogate_model']=self.surrogate_model.name
        except:
            print('No keras model attribute')
            
        return state
    
    #When unpickling, reload the model back into the object.
    def __setstate__(self, state):
        
        self.__dict__.update(state)
        try:
            self.surrogate_model = tf.keras.models.load_model(base_location+self.surrogate_model+'.h5')
        except:
            print('No keras model attribute')
            
        if state['idf']:
            self.idf = ef.get_building(base_location+'model_setup.idf',version='9.5',data_dict=base_location+'Energy+.idd')
            
        if state['parameters']:
            self.parameters = parameters_set()[0]
            
        return
    
    
    def use_surrogate_model(self,inputs):
        return self.surrogate_model(np.array([inputs]))
#         try:
#             return self.model(np.array([inputs]))
#         except:
#             print("Error: No model loaded or loaded improperly") 
        
    def set_daterange(self,monthlist=True):
        if monthlist:
            date1 = str(str(self.idf.idfobjects['RunPeriod'][0].Begin_Year)+'-'+str(self.idf.idfobjects['RunPeriod'][0].Begin_Month)+'-'+str(self.idf.idfobjects['RunPeriod'][0].Begin_Day_of_Month))  # input start date
            date2 = str(str(self.idf.idfobjects['RunPeriod'][0].End_Year)+'-'+str(self.idf.idfobjects['RunPeriod'][0].End_Month)+'-'+str(self.idf.idfobjects['RunPeriod'][0].End_Day_of_Month))  # input end date
#             self.month_list = [i.strftime("%b-%y") for i in pd.date_range(start=date1, end=date2, freq='MS')] #Convert to str showing Mon-Yr
            self.month_list = [i for i in pd.date_range(start=date1, end=date2, freq='MS')] #Use datetime object
            
            return self
        else:
            self.month_list=monthlist
            return self
        
        
    def get_properties(self):
        #use EPPY to parse our IDF

        # get the surfaces in the model, filtered by having Outdoors as the boundary condition 
        surfaces = [s for s in self.idf.idfobjects['BuildingSurface:Detailed'.upper()] 
                    if s.Outside_Boundary_Condition == 'Outdoors']    
        self.total_exterior_area = sum(s.area for s in surfaces)

        # get the surfaces in the model, filtered by having Outdoors as the boundary condition 
        walls = [s for s in self.idf.idfobjects['BuildingSurface:Detailed'.upper()] 
                    if s.Outside_Boundary_Condition == 'Outdoors' and s.Surface_Type == 'Wall']    
        self.total_wall_area = sum(s.area for s in walls)

        # get the floors in the model, filtered by having Floor as the surface type
        floors = [s for s in self.idf.idfobjects['BuildingSurface:Detailed'.upper()] 
                    if s.Surface_Type == 'Floor']    
        floor_area = sum(s.area for s in floors)
        self.floor_area = 30432 #for now use same value that was used in benchmarking report.

        # get the glazing surfaces to remove from the total external areas
        glazing = self.idf.idfobjects['FenestrationSurface:Detailed'.upper()]
        self.total_glazing_area = sum(g.area for g in glazing)
        
        return self
    
    
    
    def convert_units(self,dataset,convert_to):
        
        if convert_to=='kWh/m2':
            dataset=dataset/1000000000*277.8/self.floor_area #Convert from Joules to kWh per square meter
            print('Assumed input unit is total Joules - ')
            print('Converted from Joules to kWh/m2')
            
        elif convert_to=='kWh':
            dataset=dataset/1000000000*277.8 #Convert from Joules to kWh
            print('Assumed input unit is total Joules - ')
            print('Converted from Joules to kWh')
            
        elif convert_to=='GJ/2':
            dataset=dataset/1000000000/self.floor_area #Convert from Joules to GJ per square meter
            print('Assumed input unit is total Joules - ')
            print('Converted from Joules to GJ/m2')
            
        elif convert_to=='GJ':
            dataset=dataset/1000000000 #Convert from Joules to GJ
            print('Assumed input unit is total Joules - ')
            print('Converted from Joules to GJ')
        
        else:
            print('no unit conversion')
            
        return dataset
    
    
    
    def load_samples_out(self, samples_out, convert_to='kWh/m2'):
        self.samples_out=samples_out
#         self.get_properties()
        self.samples_out=self.convert_units(self.samples_out,convert_to)
        
        data=[]
        for row in self.samples_out.index:
            for column in self.samples_out.columns:
                data.append(list(self.samples_out.loc[row,column].values))
        data

        top_columns=list(self.meters.keys())
        second_columns = self.month_list
        rows = self.samples_out.index
        data = np.array(data).reshape(len(rows),len(second_columns) * len(top_columns))

        midx = pd.MultiIndex.from_product([top_columns, second_columns])
        self.samples_out = pd.DataFrame(data, index=rows, columns=midx)
        
        self.samples_out_wide=copy.deepcopy(self.samples_out)
        
        self.samples_out_wide=self.flatten(self.samples_out_wide)
       
                            
        return self
    
    
    def flatten(self,dataset):
        dataset_wide=pd.DataFrame()
        for meter in self.meters:
            for month in self.month_list:
                metermonth=meter+' '+str(month)
                dataset_wide[metermonth]=dataset[meter][month]
            
        return dataset_wide
    
    #Ping the surrogate model
    def evaluate_surrogate(self,inputs): #inputs in the form of a pandas dataframe, with columns representing parameter inputs
        evaluation=self.scaler_in.transform(inputs)
        evaluation=self.surrogate_model(evaluation,training=False)
        evaluation=self.scaler_out.inverse_transform(evaluation)
        evaluation=pd.DataFrame(evaluation,columns=self.samples_out.columns)
        return evaluation
    
    
    #UnNormalize the parameter from between 0 and 1 (to work with beta distribution) back into the original range. Simple transformation using min/max of accepted values. Uses kwArgs to allow flexibility
    def convert_from_minmax(self,**kwarg):

        for key in kwarg.keys(): #Run through all kwArgs submitted (parameters used)
            for i,prior in enumerate(self.priors): #Loop through all parameters in the datamodel, keeping track of index
                    if key==prior['name']: #Match kwArg to parameter
                        para=self.parameters[i]
                        var=kwarg[key] #[0,1] input
                        kwarg[key]=(var*(para.value_descriptors[0].max-para.value_descriptors[0].min)+para.value_descriptors[0].min) #Convert using ratio of max and min.

        return kwarg
    
    #Uormalize the parameter from original value range to between 0 and 1 (to work with beta distribution). Simple transformation using min/max of accepted values. Uses kwArgs to allow flexibility
    def convert_to_minmax(self,**kwarg):

        for key in kwarg.keys(): #Run through all kwArgs submitted (parameters used)
            for i,prior in enumerate(self.priors): #Loop through all parameters in the datamodel, keeping track of index
                    if key==prior['name']: #Match kwArg to parameter
                        para=self.parameters[i]
                        var=kwarg[key] #[max,min] input
                        kwarg[key]=(var-para.value_descriptors[0].min)/(para.value_descriptors[0].max-para.value_descriptors[0].min) #Convert using ratio of max and min.

        return kwarg
    
    #Create a list of inputs to be fed through the surrogate model.
    def create_input_list(self,**kwarg):
        inputs=copy.deepcopy(self.defaults) #Copy the default as the baseline going forward. This makes sure we include all parameters required into the surrogate, replacing the ones we do not care about with parameters that we set as the default. Note this value should be [Max,min] of parameter, and not normalized to [1,0]

        for key in kwarg.keys(): #Loop through each kwArg submitted. Using kwArgs allows us to be flexible in input parameters 
            for i,prior in enumerate(self.priors): #Loop through all parameters
                if key==prior['name']: #Find matching parameter to input
                    inputs[self.parameters[i].value_descriptors[0].name]=kwarg[key] #replace the default value with the input value for each chosen parameter
        return inputs
    
    def filter_outputs(self,results):
        if self.calibration_meters==None:
            self.calibration_meters=self.meters.keys()
        if self.calibration_months==None:
            self.calibration_months=self.month_list
            
        idx = pd.IndexSlice    
        df_list=[]
        for meter in self.calibration_meters:
            df_list.append(results[[meter]].loc[:, idx[:, self.calibration_months]])
        filtered_results=pd.concat(df_list,axis=1)    
            
        return filtered_results
    
    
    
    def pymc_evaluate(self,**kwargs): #This method runs through the process of converting the [0,1] inputs to  the associated [Min,Max] for each parameter. It then creates an input dataframe using the defaults as the basis, with provided parameters/values replacing those in the defaults. Finally it returns a numpy array compatible with pymc3.
        
        converted=self.convert_from_minmax(**kwargs)
        input_list=self.create_input_list(**converted)
        results=self.evaluate_surrogate(input_list)
        filtered_results=self.filter_outputs(results)
        return filtered_results.to_numpy()[0]
    
    def evaluate(self,**kwargs): #This method is identical to pymc_evaluate, but it does not convert from [0,1] to [min,max], and assumes it is in the current [minmax] basis.

        input_list=self.create_input_list(**kwargs)
        results=self.evaluate_surrogate(input_list)
        filtered_results=self.filter_outputs(results)
        return filtered_results
    
    #Create a lineplot comparing the observed, the prior estimates, and the posterior estimates for a certain meter and daterange
    def plot_prior_post_observed(self,meter,daterange=None,ylow=None,yhigh=None,aspect=6):
        if daterange is None:
            daterange=self.calibration_months
            
        obs=self.filter_outputs(self.observed)[meter][daterange]
        ax=sns.relplot(kind='line',x=obs.columns,y=obs.iloc[0],aspect=aspect,color='red')
        sns.lineplot(data=self.prior_sims[meter][daterange].melt(),x='variable',y='value',n_boot=1000,ci='sd',color='blue')
        sns.lineplot(data=self.posterior_sims[meter][daterange].melt(),x='variable',y='value',n_boot=1000,ci='sd',color='orange')
        plt.legend(labels=['Observed','Prior','Posterior'])
        ax.fig.suptitle(meter)
        ax.ax.set(ylim=(ylow, yhigh))
        
        return ax
    
    # Plot the prior distribution and posterior distribution of certain parameters
    def plot_inference(self,cols=4,label1='Prior',label2='Posterior'):
        
        rows=math.ceil(len(self.prior_checks.columns)/cols)

        fig = plt.figure(figsize=[cols*5,rows*5])
        fig.subplots_adjust(hspace=0.4, wspace=0.4)
        for i,name in enumerate(self.prior_checks.columns):
            ax = fig.add_subplot(rows, cols, i+1)
            sns.kdeplot(self.prior_checks[name],ax=ax,shade=True,label=label1)
            sns.kdeplot(self.posterior_checks[name],ax=ax,shade=True,color='orange',label=label2)
                    
        return fig
    
    
    def update_prior(self,parameter,plot=False):
        a,b,loc,scale=beta.fit(data=self.convert_to_minmax(**self.posterior_checks)[parameter],floc=0,fscale=1)
        
        prior=next((prior for prior in self.priors if prior['name'] == parameter), None)
        prior['alpha']=a
        prior['beta']=b
        
        if plot:
            plt.hist(self.convert_to_minmax(**self.posterior_checks)[parameter],bins=10);
            r = beta.rvs(a, b, size=len(self.convert_to_minmax(**self.posterior_checks)[parameter]))
            plt.hist(r,bins=10);
            plt.title(parameter)
            plt.show()
            
        return self
    
    def update_all_priors(self,parameter_list,plot=False):
        for parameter in parameter_list:
            self.update_prior(parameter,plot)
        return 
    
    def load_prior_posterior(self,prior_checks,ppc):
        
        prior_checks=pd.DataFrame(prior_checks)
        posterior_inputs=ppc.copy()
        posterior_inputs.pop('sim')
        posterior_inputs=pd.DataFrame(posterior_inputs)

        self.prior_checks=pd.DataFrame(self.convert_from_minmax(**prior_checks))
        self.posterior_checks=pd.DataFrame(self.convert_from_minmax(**posterior_inputs))

        self.prior_sims=pd.DataFrame(columns=self.evaluate(**self.prior_checks).columns)
        for i,row in self.prior_checks.iterrows():
            self.prior_sims=self.prior_sims.append(self.evaluate(**row))

        self.posterior_sims=pd.DataFrame(columns=self.evaluate(**self.prior_checks).columns)
        for i,row in self.posterior_checks.iterrows():
            self.posterior_sims=self.posterior_sims.append(self.evaluate(**row))
            
        return self
    
    def robust_evaluate(self,inferred_paras=[],**kwargs):
        for inferred in inferred_paras:
            prior=next((prior for prior in self.priors if prior['name'] == inferred), None)
            rv=beta.rvs(prior['alpha'],prior['beta'])
            kwargs[inferred]=rv
        return self.pymc_evaluate(**kwargs)

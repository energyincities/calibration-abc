import pickle
import datetime
import copy
import pandas as pd
pd.set_option('display.max_columns', None)
import numpy as np
import math
import sys

import matplotlib.pyplot as plt
import seaborn as sns

from besos.parameters import wwr, RangeParameter, FieldSelector, FilterSelector, CategoryParameter, GenericSelector, Parameter, expand_plist, Descriptor
from besos.parameters import RangeParameter, CategoryParameter, expand_plist
from besos.objectives import MeterReader, time_series_values

from besos import eppy_funcs as ef
import besos.sampling as sampling
from besos.evaluator import EvaluatorEP
from besos.problem import EPProblem

from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score, mean_absolute_error,mean_absolute_percentage_error

import scipy
from scipy.stats import beta

import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras import layers
from tensorflow.keras import models
from tensorflow.keras.utils import plot_model
from tensorflow.keras.layers import Dense, LeakyReLU, Activation, Dropout
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import ELU    
from tensorflow.keras.models import Sequential

import pymc3 as pm
import arviz as az

az.style.use("arviz-darkgrid")

from base_location import *
from parameters_set import parameters_set

from datamodel_class import DataModel

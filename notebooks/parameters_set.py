from besos.parameters import wwr, RangeParameter, FieldSelector, FilterSelector, CategoryParameter, GenericSelector, Parameter, expand_plist, Descriptor
from besos.parameters import RangeParameter, CategoryParameter, expand_plist
from besos.objectives import MeterReader, time_series_values

from besos import eppy_funcs as ef
import besos.sampling as sampling
from besos.evaluator import EvaluatorEP
from besos.problem import EPProblem

import copy
import pandas as pd
pd.set_option('display.max_columns', None)
import numpy as np
import pickle

base_location='/home/user/Major Projects/BOMA Project/Models for Calibration/Bay Centre/'



def parameters_set():
    
    common_zone_list=['Atrium','Cafeteria','Main Corridors','Periphery Corridors','Vestibule','Parking']
    tenant_zone_list=['Retail','ServiceCanada','Fitness','Food']

    parameters=[]
    priors=[]
    defaults=[]

    #Effective R-value of insulation. Does not include conventive coefficients or thermal resistance of non-insulation materials. Assume expected value currently is R-4 of insulation
    
    def wall_r_value(building,value):
            ins_rvalue=value
            ins_rsivalue=ins_rvalue/5.678
#             ins_thickness=ins_rsivalue*building.getobject(key='Material',name='Typical Wall Insulation').Conductivity
            ins_thickness=ins_rsivalue*building.getobject(key='Material',name='Generic 50mm Insulation').Conductivity
#             FieldSelector(class_name='Material',object_name='Typical Wall Insulation',field_name='Thickness').set(building,ins_thickness)
            FieldSelector(class_name='Material',object_name='Generic 50mm Insulation',field_name='Thickness').set(building,ins_thickness)
            return

    rval=Parameter(name='Wall Insulation (R-Value)',
                               selector=GenericSelector(set=wall_r_value),
                               value_descriptor=RangeParameter(min_val=1,max_val=10))
    parameters.append(rval)  
    defaults.append(4.2)

    prior_data={
        'name':'rval',
        'distribution':'beta',
        'alpha':3,
        'beta':5
    }

    priors.append(prior_data)
    
    #Infilatration rate of the whole building
    
    inf=Parameter(name='Wall Infiltration (m3/s-m2)',
                               selector=FieldSelector(class_name='ZoneInfiltration:DesignFlowRate',
                                                      object_name='*',field_name='Flow per Exterior Surface Area'),
                               value_descriptor=RangeParameter(min_val=0.00005,max_val=0.0005))
    parameters.append(inf)  
    defaults.append(0.0003)

    prior_data={
        'name':'inf',
        'distribution':'beta',
        'alpha':2,
        'beta':2
    }

    priors.append(prior_data)
    
    # Recently redid roof, assume 4 to 6 inches of EPS or Polyiso. R4.5/inch/R6.8/inch -> R18 to R40
    
    def roof_r_value(building,value):
            ins_rvalue=value
            ins_rsivalue=ins_rvalue/5.678
#             ins_thickness=ins_rsivalue*building.getobject(key='Material',name='Typical Wall Insulation').Conductivity
            ins_thickness=ins_rsivalue*building.getobject(key='Material',name='Generic Roof Insulation').Conductivity
#             FieldSelector(class_name='Material',object_name='Typical Wall Insulation',field_name='Thickness').set(building,ins_thickness)
            FieldSelector(class_name='Material',object_name='Generic Roof Insulation',field_name='Thickness').set(building,ins_thickness)
            return

    roof=Parameter(name='Roof Insulation (R-Value)',
                               selector=GenericSelector(set=roof_r_value),
                               value_descriptor=RangeParameter(min_val=18,max_val=40))
    parameters.append(roof)  
    defaults.append(25)

    prior_data={
        'name':'roof',
        'distribution':'beta',
        'alpha':3,
        'beta':5
    }

    priors.append(prior_data)
    

    #Glazing USI- this is directly, W/m2K of the glazing system as a whole. Currently set to modify all windows equally. Assume USI of 2.5 currently as MLE. 

    usi=Parameter(name='Glazing U-value (W/m2K)',
                               selector=FieldSelector(class_name='WindowMaterial:SimpleGlazingSystem',
                                                      object_name='*',field_name='U-Factor'),
                               value_descriptor=RangeParameter(min_val=2,max_val=4))
    parameters.append(usi)

    defaults.append(2.7)

    prior_data={
        'name':'usi',
        'distribution':'beta',
        'alpha':2,
        'beta':3
    }

    priors.append(prior_data)

    #Glazing SHGC this is directly the glazing system as a whole. Currently set to modify all windows equally. Assume SHGC of 0.5 as prior MLE. 

    shgc=Parameter(name='Glazing SHGC',
                               selector=FieldSelector(class_name='WindowMaterial:SimpleGlazingSystem',
                                                      object_name='*',field_name='Solar Heat Gain Coefficient'),
                               value_descriptor=RangeParameter(min_val=0.2,max_val=0.8))
    parameters.append(shgc)
    defaults.append(.5)

    prior_data={
        'name':'shgc',
        'distribution':'beta',
        'alpha':2,
        'beta':2
    }

    priors.append(prior_data)
    
    #Common zone lighting, multiplying the base LPD (NECB 2015) by a factor. 
    
    def common_lights(building,value):
            multiplier=value
            for zone in common_zone_list:
                lpd=multiplier*building.getobject(key='Lights',name=zone+' Lights').Watts_per_Zone_Floor_Area
                FieldSelector(class_name='Lights',object_name=zone+' Lights',field_name='Watts_per_Zone_Floor_Area').set(building,lpd)
            return

    common_lpd=Parameter(name='Common Lights (Multiplier)',
                               selector=GenericSelector(set=common_lights),
                               value_descriptor=RangeParameter(min_val=0.1,max_val=2))
    parameters.append(common_lpd)  
    defaults.append(0.8)

    prior_data={
        'name':'common_lpd',
        'distribution':'beta',
        'alpha':2,
        'beta':3
    }

    priors.append(prior_data)
    
    #Tenant zone lighting, multiplying the base LPD (NECB 2015) by a factor. 
    
    def tenant_lights(building,value):
            multiplier=value
            for zone in tenant_zone_list:
                lpd=multiplier*building.getobject(key='Lights',name=zone+' Lights').Watts_per_Zone_Floor_Area
                FieldSelector(class_name='Lights',object_name=zone+' Lights',field_name='Watts_per_Zone_Floor_Area').set(building,lpd)
            return

    tenant_lpd=Parameter(name='Tenant Lights (Multiplier)',
                               selector=GenericSelector(set=tenant_lights),
                               value_descriptor=RangeParameter(min_val=0.1,max_val=2))
    parameters.append(tenant_lpd)  
    defaults.append(0.7)

    prior_data={
        'name':'tenant_lpd',
        'distribution':'beta',
        'alpha':2,
        'beta':3
    }

    priors.append(prior_data)
    
    #Modify the lighting schedule between NECB-C and always-on by a relative fraction
    
    def lighting_sched(building,value):
        #Get NECB-C Values
        val_1=float(building.getobject(key='Schedule:Day:Interval',name='NECB-C-Lighting_Default').Value_Until_Time_1)
        val_1=value*(1-val_1)+val_1
        val_2=float(building.getobject(key='Schedule:Day:Interval',name='NECB-C-Lighting_Default').Value_Until_Time_2)
        val_2=value*(1-val_2)+val_2
        val_3=float(building.getobject(key='Schedule:Day:Interval',name='NECB-C-Lighting_Default').Value_Until_Time_3)
        val_3=value*(1-val_3)+val_3
        val_4=float(building.getobject(key='Schedule:Day:Interval',name='NECB-C-Lighting_Default').Value_Until_Time_4)
        val_4=value*(1-val_4)+val_4
        val_5=float(building.getobject(key='Schedule:Day:Interval',name='NECB-C-Lighting_Default').Value_Until_Time_5)
        val_5=value*(1-val_5)+val_5
        val_6=float(building.getobject(key='Schedule:Day:Interval',name='NECB-C-Lighting_Default').Value_Until_Time_6)
        val_6=value*(1-val_6)+val_6
        #Multiply by a factor between NECB-C and on 24/7
        FieldSelector(class_name='Schedule:Day:Interval',object_name='NECB-C-Lighting_Default',field_name='Value Until Time 1').set(building,val_1)
        FieldSelector(class_name='Schedule:Day:Interval',object_name='NECB-C-Lighting_Default',field_name='Value Until Time 2').set(building,val_2)
        FieldSelector(class_name='Schedule:Day:Interval',object_name='NECB-C-Lighting_Default',field_name='Value Until Time 3').set(building,val_3)
        FieldSelector(class_name='Schedule:Day:Interval',object_name='NECB-C-Lighting_Default',field_name='Value Until Time 4').set(building,val_4)
        FieldSelector(class_name='Schedule:Day:Interval',object_name='NECB-C-Lighting_Default',field_name='Value Until Time 5').set(building,val_5)
        FieldSelector(class_name='Schedule:Day:Interval',object_name='NECB-C-Lighting_Default',field_name='Value Until Time 6').set(building,val_6)
        
        return
    
    lighting_schedule=Parameter(name='Lighting Schedule',
                               selector=GenericSelector(set=lighting_sched),
                               value_descriptor=RangeParameter(min_val=0,max_val=1))
    parameters.append(lighting_schedule)  
            
    
    defaults.append(0.3)

    prior_data={
        'name':'lighting_schedule',
        'distribution':'beta',
        'alpha':2,
        'beta':3
    }

    priors.append(prior_data)
    
    
    
    
        #Common zone plug loads, multiplying the base EPD (NECB 2015) by a factor. 
    
    def common_plug(building,value):
            multiplier=value
            for zone in common_zone_list:
                epd=multiplier*building.getobject(key='ElectricEquipment',name=zone+' Plug').Watts_per_Zone_Floor_Area
                FieldSelector(class_name='ElectricEquipment',object_name=zone+' Plug',field_name='Watts_per_Zone_Floor_Area').set(building,epd)
            return

    common_plug=Parameter(name='Common Plug Loads (Multiplier)',
                               selector=GenericSelector(set=common_plug),
                               value_descriptor=RangeParameter(min_val=0.3,max_val=3))
    parameters.append(common_plug)  
    defaults.append(1.7)

    prior_data={
        'name':'common_plug',
        'distribution':'beta',
        'alpha':2,
        'beta':2
    }

    priors.append(prior_data)
    
    #Tenant zone plug loads, multiplying the base EPD (NECB 2015) by a factor. 
    
    def tenant_plug(building,value):
            multiplier=value
            for zone in tenant_zone_list:
                epd=multiplier*building.getobject(key='ElectricEquipment',name=zone+' Plug').Watts_per_Zone_Floor_Area
                FieldSelector(class_name='ElectricEquipment',object_name=zone+' Plug',field_name='Watts_per_Zone_Floor_Area').set(building,epd)
            return

    tenant_plug=Parameter(name='Tenant Plug Loads (Multiplier)',
                               selector=GenericSelector(set=tenant_plug),
                               value_descriptor=RangeParameter(min_val=0.3,max_val=3))
    parameters.append(tenant_plug)  
    defaults.append(1.7)

    prior_data={
        'name':'tenant_plug',
        'distribution':'beta',
        'alpha':2,
        'beta':2
    }

    priors.append(prior_data)
    
    #Ventilation requirement, multiplied by a factor for sizing total ventilation intake. 
    
    def ventilation_rate(building,value):
            multiplier=value
            for obj in building.idfobjects['DesignSpecification:OutdoorAir']:
                oa_person=multiplier*float(building.getobject(key='DesignSpecification:OutdoorAir',name=obj.Name).Outdoor_Air_Flow_per_Person)
                oa_floor=multiplier*float(building.getobject(key='DesignSpecification:OutdoorAir',name=obj.Name).Outdoor_Air_Flow_per_Zone_Floor_Area)
                FieldSelector(class_name='DesignSpecification:OutdoorAir',object_name=obj.Name,field_name='Outdoor_Air_Flow_per_Person').set(building,oa_person)
                FieldSelector(class_name='DesignSpecification:OutdoorAir',object_name=obj.Name,field_name='Outdoor_Air_Flow_per_Zone_Floor_Area').set(building,oa_floor)
            return

    ventilation_rate=Parameter(name='Ventilation Rate (Multiplier)',
                               selector=GenericSelector(set=ventilation_rate),
                               value_descriptor=RangeParameter(min_val=0.5,max_val=2))
    parameters.append(ventilation_rate)  
    defaults.append(1.5)

    prior_data={
        'name':'ventilation_rate',
        'distribution':'beta',
        'alpha':3,
        'beta':2
    }

    priors.append(prior_data)
    
    #Chiller nominal COP.
    
    chiller_cop=Parameter(name='Main Chiller COP',
                               selector=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chiller_1',field_name='Reference COP'),
                               value_descriptor=RangeParameter(min_val=4,max_val=7))
    parameters.append(chiller_cop)  
    defaults.append(5.7)

    prior_data={
        'name':'chiller_cop',
        'distribution':'beta',
        'alpha':2,
        'beta':2
    }

    priors.append(prior_data)

#     #Cooling tower minimum fan speed

#     tower_variable=Parameter(name='Cooling Tower Variable Speed Fans',
#                                selector=FieldSelector(class_name='CoolingTower:VariableSpeed',object_name='Cooling Tower Variable Speed 1',field_name='Minimum Air Flow Rate Ratio'),
#                                value_descriptor=RangeParameter(min_val=0.2,max_val=0.5))
#     parameters.append(tower_variable)  
#     defaults.append(0.5)

#     prior_data={
#         'name':'tower_variable',
#         'distribution':'beta',
#         'alpha':50,
#         'beta':1
#     }

#     priors.append(prior_data)


    #Chilled water loop minimum pump speed

#     chilled_variable=Parameter(name='Chilled Water Variable Pumping',
#                                selector=FieldSelector(class_name='Pump:VariableSpeed',object_name='CHWL_Pump',field_name='Design Minimum Flow Rate Fraction'),
#                                value_descriptor=RangeParameter(min_val=0.1,max_val=1))
#     parameters.append(chilled_variable)  
#     defaults.append(1)

#     prior_data={
#         'name':'chilled_variable',
#         'distribution':'beta',
#         'alpha':50,
#         'beta':1
#     }

#     priors.append(prior_data)

#     #Condenser water loop minimum pump speed

#     condenser_variable=Parameter(name='Condenser Water Variable Pumping',
#                                selector=FieldSelector(class_name='Pump:VariableSpeed',object_name='CWL_Pump',field_name='Design Minimum Flow Rate Fraction'),
#                                value_descriptor=RangeParameter(min_val=0.1,max_val=1))
#     parameters.append(condenser_variable)  
#     defaults.append(1)

#     prior_data={
#         'name':'condenser_variable',
#         'distribution':'beta',
#         'alpha':50,
#         'beta':1
#     }

#     priors.append(prior_data)

    #Cooking load
    
    cooking=Parameter(name='Cooking Energy',
                               selector=FieldSelector(class_name='GasEquipment',object_name='Food Gas',field_name='Power per Zone Floor Area'),
                               value_descriptor=RangeParameter(min_val=5,max_val=200))
    parameters.append(cooking)  
    defaults.append(100)

    prior_data={
        'name':'cooking',
        'distribution':'beta',
        'alpha':1,
        'beta':1
    }

    priors.append(prior_data)
    
    #Elevator Power
    
    elevators=Parameter(name='Elevator Power',
                               selector=FieldSelector(class_name='ElectricEquipment',object_name='Elevators',field_name='Design Level'),
                               value_descriptor=RangeParameter(min_val=20000,max_val=40000))
    parameters.append(elevators)  
    defaults.append(30000)

    prior_data={
        'name':'elevators',
        'distribution':'beta',
        'alpha':2,
        'beta':2
    }

    priors.append(prior_data)
    
    #RTU gas efficiency
    
    rtu_efficiency=Parameter(name='RTU Gas Efficiency',
                               selector=FieldSelector(class_name='Coil:Heating:Fuel',object_name='*',field_name='Burner Efficiency'),
                               value_descriptor=RangeParameter(min_val=0.6,max_val=0.9))
    parameters.append(rtu_efficiency)  
    defaults.append(0.75)

    prior_data={
        'name':'rtu_efficiency',
        'distribution':'beta',
        'alpha':2,
        'beta':2
    }

    priors.append(prior_data)
    
    #SHW gas efficiency
    
    shw_efficiency=Parameter(name='SHW Gas Efficiency',
                               selector=FieldSelector(class_name='Boiler:HotWater',object_name='SHW Heater',field_name='Nominal_Thermal_Efficiency'),
                               value_descriptor=RangeParameter(min_val=0.6,max_val=0.98))
    parameters.append(shw_efficiency)  
    defaults.append(0.8)

    prior_data={
        'name':'shw_efficiency',
        'distribution':'beta',
        'alpha':2,
        'beta':2
    }

    priors.append(prior_data)
    
    
    #FCU Fan Pressure - represents FCU fan power
    
    fcu_pressure=Parameter(name='FCU Fan Pressure',
                               selector=FieldSelector(class_name='Fan:VariableVolume',object_name='*',field_name='Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=75,max_val=500))
    parameters.append(fcu_pressure)  
    defaults.append(275)

    prior_data={
        'name':'fcu_pressure',
        'distribution':'beta',
        'alpha':1,
        'beta':1
    }

    priors.append(prior_data)
    
    #Variable to allow variable-speed fan coil fans
    
    fcu_variable=Parameter(name='FCU Variable',
                               selector=FieldSelector(class_name='ZoneHVAC:FourPipeFanCoil',object_name='*',field_name='Low Speed Supply Air Flow Ratio'),
                               value_descriptor=RangeParameter(min_val=0.3,max_val=0.9))
    parameters.append(fcu_variable)  
    defaults.append(0.85)

    prior_data={
        'name':'fcu_variable',
        'distribution':'beta',
        'alpha':10,
        'beta':2
    }

    priors.append(prior_data)
    
    #DOAS system fan pressure - represents total fan power
    
    doas_pressure=Parameter(name='DOAS Fan Pressure',
                               selector=FieldSelector(class_name='Fan:VariableVolume',object_name='Common_DOAS_fan',field_name='Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=250,max_val=1000))
    parameters.append(doas_pressure)  
    defaults.append(750)

    prior_data={
        'name':'doas_pressure',
        'distribution':'beta',
        'alpha':3,
        'beta':2
    }

    priors.append(prior_data)

    
    #Modify the cooling setpoint
    
    cooling_setpoint=Parameter(name='Cooling Setpoint',
                               selector=FieldSelector(class_name='Schedule:Day:Interval',object_name='NECB-C-Thermostat Setpoint-Cooling_Default',field_name='Value Until Time 2'),
                               value_descriptor=RangeParameter(min_val=20.5,max_val=22))
    parameters.append(cooling_setpoint)  
    defaults.append(21.5)

    prior_data={
        'name':'cooling_setpoint',
        'distribution':'beta',
        'alpha':2,
        'beta':2
    }

    priors.append(prior_data)
    
    #Modify the heating setpoint
    
    heating_setpoint=Parameter(name='Heating Setpoint',
                               selector=FieldSelector(class_name='Schedule:Day:Interval',object_name='NECB-C-Thermostat Setpoint-Heating_Default',field_name='Value Until Time 3'),
                               value_descriptor=RangeParameter(min_val=18,max_val=20))
    parameters.append(heating_setpoint)  
    defaults.append(19)

    prior_data={
        'name':'heating_setpoint',
        'distribution':'beta',
        'alpha':2,
        'beta':2
    }

    priors.append(prior_data)
    
    #Modify infiltration in vestibule to represent airflow through open doors
    
    vestibule_infiltration=Parameter(name='Vestibule Infiltration',
                               selector=FieldSelector(class_name='ZoneInfiltration:DesignFlowRate',object_name='Vestibule Infiltration',field_name='Flow per Exterior Surface Area'),
                               value_descriptor=RangeParameter(min_val=0.00025,max_val=0.025))
    parameters.append(vestibule_infiltration)  
    defaults.append(0.01)

    prior_data={
        'name':'vestibule_infiltration',
        'distribution':'beta',
        'alpha':2,
        'beta':2
    }

    priors.append(prior_data)
    
    #The annual temperature increase simplification of roof air intake
    
    roof_temp=Parameter(name='Roof Temperature Increase',
                               selector=FieldSelector(class_name='Schedule:Constant',object_name='RoofTempIncSched',field_name='Hourly Value'),                               value_descriptor=RangeParameter(min_val=0,max_val=3))
    parameters.append(roof_temp)  
    defaults.append(1.5)

    prior_data={
        'name':'roof_temp',
        'distribution':'beta',
        'alpha':2,
        'beta':2
    }

    priors.append(prior_data)
    
    
    return parameters, defaults, priors

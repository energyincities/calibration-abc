# Calibration ABC

Project demonstrating the calibration of a building following Approximate Bayesian Computation

## Authors and acknowledgment
Kevin Cant, University of Victoria

## License
Creative Commons Attribution 4.0 International

## How to Cite
Please cite the following DOI: 10.5281/zenodo.6845853
